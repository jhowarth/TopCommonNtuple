#ifndef TOPDILEPTONEVENTSAVER_H_
#define TOPDILEPTONEVENTSAVER_H_

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TRandom3.h"
#include "TH2.h"
#include "TopEventSelectionTools/PlotManager.h"

class TopDileptonEventSaver : public top::EventSaverFlatNtuple {

public:

  ///-- Default - so root can load based on a name --///
  TopDileptonEventSaver();

  ///-- Default - so we can clean up --///
  //~TopDileptonEventSaver();//=0;
  
  ///-- Exposition of base class initializers (to avoid [-Woverloaded-virtual] hidden warnings --///
  using top::EventSaverFlatNtuple::initialize;
  
  ///-- Run once at the start of the job --///
  void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches);

  ///-- Run for every event (in every systematic) that needs saving --///
  void initializeRecoLevelBranches();
  void saveEvent(const top::Event& event);

  //skip saving nominal branch - useful for splitting ntuples by systematics if need be
  int isBranchStored(top::TreeManager const *treeManager, const std::string &variableName);
  bool m_skipNominal;

  ///-- For particle level object --///
  void setupParticleLevelTreeManager(const top::ParticleLevelEvent& plEvent);
  void saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent);
  void initializeParticleLevelBranches();

  ///-- For parton level objects --///
  void initializePartonLevelEvent();
  void setupPartonLevelEvent();
  void saveTruthEvent();
  
  ///-- Functions for event selection flags --///
  bool passEMu();
  bool passEMu_pretag();
  bool passEMu_control();
  bool passParticleEMu();
  bool passEE();
  bool passEE_pretag();
  bool passEE_control();
  bool passParticleEE();
  bool passMuMu();
  bool passMuMu_pretag();
  bool passMuMu_control();
  bool passParticleMuMu();

 private:

  ///-- Steering --///
  bool is_MC;
  std::shared_ptr<top::TopConfig> m_config;
  std::shared_ptr<top::TreeManager> m_NominalTreeManager;
  std::shared_ptr<top::TreeManager> m_truthTreeManager;

  ///-- Extra variable to write out --///
  int m_lep_n;

  TLorentzVector m_parton_l;
  TLorentzVector m_parton_lbar;

  float m_parton_l_pt;
  float m_parton_l_eta;
  float m_parton_l_phi;
  float m_parton_l_e;
  int m_parton_l_pdgId;

  float m_parton_lbar_pt;
  float m_parton_lbar_eta;
  float m_parton_lbar_phi;
  float m_parton_lbar_e;
  int m_parton_lbar_pdgId;

  
  std::vector<float> m_lep_pt;
  std::vector<float> m_lep_eta;
  std::vector<float> m_lep_phi;
  std::vector<float> m_lep_e;
  std::vector<float> m_lep_type;
  std::vector<float> m_lep_charge;

  std::vector<int>   m_lep_truth_type;

  std::vector<int>   m_lep_iso_gradient;
  std::vector<int>   m_lep_iso_gradient_loose;

  std::vector<char>   m_lep_id_medium;
  std::vector<char>   m_lep_id_tight;

  int m_particle_lep_n;

  std::vector<float> m_particle_lep_pt;
  std::vector<float> m_particle_lep_eta;
  std::vector<float> m_particle_lep_phi;
  std::vector<float> m_particle_lep_e;
  std::vector<float> m_particle_lep_type;
  std::vector<float> m_particle_lep_charge;

  //ClassDef(top::TopDileptonEventSaver, 1);
  ClassDef(TopDileptonEventSaver, 0);
  };

  //}

#endif

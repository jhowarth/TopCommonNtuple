/*
Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "TopCommonDilepton/TopDileptonEventSaver.h"
#include "TopCommonDilepton/NeutrinoWeighter.h"

#ifdef __CINT__

#pragma extra_include "TopCommonDilepton/TopDileptonEventSaver.h";
#pragma extra_include "TopCommonDilepton/NeutrinoWeighter.h";

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class NeutrinoWeighter+;
#pragma link C++ class TopDileptonEventSaver+;
//#pragma link C++ class top::CustomObjectLoader+;
//#pragma link C++ class top::HowtoExtendAnalysisTopLoader+;
//#pragma link C++ class top::CustomEventSaver+;

#endif

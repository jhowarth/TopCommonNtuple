#include "TopCommonDilepton/TopDileptonEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopParticleLevel/ParticleLevelEvent.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEventSelectionTools/PlotManager.h"

#include "PathResolver/PathResolver.h"

#include "TopConfiguration/ConfigurationSettings.h"

#include "TopEvent/TopEventMaker.h"
#include "TopEvent/Event.h"

#include "TH1.h"
#include "TH2.h"
#include "TopCommonDilepton/NeutrinoWeighter.h"

//namespace top {

///-- Always initialise primitive types in the constructor --///
TopDileptonEventSaver::TopDileptonEventSaver() : 
  is_MC(true),
  m_NominalTreeManager(nullptr),
  m_truthTreeManager(nullptr)
{ 
  top::ConfigurationSettings* configSettings = top::ConfigurationSettings::get();
  m_skipNominal = (configSettings->value("SkipNominalTree") == "True"); //this is a custom variable for if we ever need to separate ntuples by systematics
  branchFilters().push_back(std::bind(&TopDileptonEventSaver::isBranchStored, this, std::placeholders::_1, std::placeholders::_2));
}

void TopDileptonEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file,const std::vector<std::string>& extraBranches) {

  m_config = config;
  EventSaverFlatNtuple::initialize(config, file, extraBranches);

  if (config->isMC()){
    is_MC = true;
    m_truthTreeManager = EventSaverFlatNtuple::truthTreeManager();
  } else {
    is_MC = false;
  }

  ///////////////////////////////////////////////////////////
  ///-- Add our extra variable to each systematic TTree --///
  ///////////////////////////////////////////////////////////

  for (auto systematicTree : treeManagers()) {

    //this is where we made reco level variables previously

    systematicTree->makeOutputVariable(m_lep_n,                         "d_lep_n");
    systematicTree->makeOutputVariable(m_lep_pt,                        "d_lep_pt");
    systematicTree->makeOutputVariable(m_lep_eta,                       "d_lep_eta");
    systematicTree->makeOutputVariable(m_lep_phi,                       "d_lep_phi");
    systematicTree->makeOutputVariable(m_lep_e,                         "d_lep_e");
    systematicTree->makeOutputVariable(m_lep_type,                      "d_lep_type");
    systematicTree->makeOutputVariable(m_lep_charge,                    "d_lep_charge");
    systematicTree->makeOutputVariable(m_lep_iso_gradient,              "d_lep_iso_gradient");
    systematicTree->makeOutputVariable(m_lep_iso_gradient_loose,        "d_lep_iso_gradient_loose");
    systematicTree->makeOutputVariable(m_lep_id_medium,                 "d_lep_id_medium");
    systematicTree->makeOutputVariable(m_lep_id_tight,                  "d_lep_id_tight");

  } /// End of tree-manager loop 
  if(is_MC){
    setupPartonLevelEvent(); // parton stuff was done here previously - it's exactly the same but put into a different function for no reason...
  }

  if ( topConfig()->doTopParticleLevel() ){
    //EventSaverFlatNtuple::setupParticleLevelTreeManager(plEvent);                                                                                            

    //this is where we made particle level variables previously
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_n,                "d_lep_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_pt,               "d_lep_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_eta,              "d_lep_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_phi,              "d_lep_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_e,                "d_lep_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_type,             "d_lep_type");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_charge,           "d_lep_charge");

  }
}

void TopDileptonEventSaver::saveEvent(const top::Event& event){

  //here we go doing reco initialisation
  initializeRecoLevelBranches();

  TLorentzVector lep_pos, lep_neg;


  ///////////////////////////////////////
  ///--       Lepton Selection      --///
  ///////////////////////////////////////

  ///-- Gonna need the primary vertex for some lepton selections --///
  const xAOD::VertexContainer* m_primary_vertices = event.m_primaryVertices;
  int n_vertices = 0;
  //float primary_vertex_z = 0.;
  for (const auto* const vertex: *m_primary_vertices) {
    const xAOD::VxType::VertexType vtype = vertex->vertexType();
    const int vmult = vertex->trackParticleLinks().size();
    // count vertices of type 1 (primary) and 3 (pileup) with >= 5 tracks
    if ((vtype == 1 || vtype == 3) && vmult >= 5) {
      ++n_vertices;
      // assuming there is only one primary vertex
      if (vtype == 1) {/*primary_vertex_z = vertex->z();*/}
    }
  }						       

  ///-- Electrons --///
  for (const auto* const electron : event.m_electrons) {    
    m_lep_pt.push_back(electron->pt()/1000.);
    m_lep_eta.push_back(electron->eta());
    m_lep_phi.push_back(electron->phi());
    m_lep_e.push_back(electron->e()/1000.);
    m_lep_type.push_back(11);
    m_lep_charge.push_back(electron->charge());

    if( electron->charge() > 0){
      lep_pos.SetPtEtaPhiE(electron->pt()/1000., electron->eta(), electron->phi(), electron->e()/1000.);
    } else if ( electron->charge() < 0){
      lep_neg.SetPtEtaPhiE(electron->pt()/1000., electron->eta(), electron->phi(), electron->e()/1000.);      
    } else {
      std::cout << "WARNING: Electron has no charge" << std::endl;
    }

    m_lep_iso_gradient.push_back(       int(electron->auxdataConst<char>("AnalysisTop_Isol_Gradient")));
    m_lep_iso_gradient_loose.push_back( int(electron->auxdataConst<char>("AnalysisTop_Isol_GradientLoose")));
    m_lep_id_medium.push_back( char(electron->auxdataConst<char>("DFCommonElectronsLHMedium")));
    m_lep_id_tight.push_back ( char(electron->auxdataConst<char>("DFCommonElectronsLHTight")));

    if(is_MC){
      if(electron->isAvailable<int>("truthType")){
	m_lep_truth_type.push_back(electron->auxdata<int>("truthType"));
        //if(electron->auxdata<int>("truthType") != 2) m_fakeEvent = true;
      } else {
	m_lep_truth_type.push_back(-99.);
      }
    } else {
      m_lep_truth_type.push_back(-99.);
    }

    ++m_lep_n;
  } // End of electrons

  ///-- Muons --///
  for (const auto* const muon : event.m_muons) {

    m_lep_pt.push_back(muon->pt()/1000.); //muon->auxdata< float >( "InnerDetectorPt" )/1000.);
    m_lep_eta.push_back(muon->eta());
    m_lep_phi.push_back(muon->phi());
    m_lep_e.push_back(muon->e()/1000.);
    m_lep_type.push_back(13);
    m_lep_charge.push_back(muon->charge());

    if( muon->charge() > 0){
      lep_pos.SetPtEtaPhiE(muon->pt()/1000., muon->eta(), muon->phi(), muon->e()/1000.);
    } else if ( muon->charge() < 0){
      lep_neg.SetPtEtaPhiE(muon->pt()/1000., muon->eta(), muon->phi(), muon->e()/1000.);      
    } else {
      std::cout << "WARNING: Muon has no charge" << std::endl;
    }

    m_lep_iso_gradient.push_back(       int(muon->auxdataConst<char>("AnalysisTop_Isol_Gradient")));
    m_lep_iso_gradient_loose.push_back( int(muon->auxdataConst<char>("AnalysisTop_Isol_GradientLoose")));
    
    if(is_MC){
      if (muon->primaryTrackParticle()){
	if(muon->primaryTrackParticle()->isAvailable<int>("truthType")){
	  m_lep_truth_type.push_back(muon->primaryTrackParticle()->auxdata<int>("truthType"));
	  //if(muon->primaryTrackParticle()->auxdata<int>("truthType") != 6) m_fakeEvent = true;
	} else {
	  m_lep_truth_type.push_back(-99.);
	}
      } else {
	m_lep_truth_type.push_back(-99.);
      }
    }

    ++m_lep_n;
  }


  //this is where we looked at jets and ran the NW previously

  ///-- Oh yeah, we want to set all the original variables too and the call fill on the TTree --///
  EventSaverFlatNtuple::saveEvent(event);
}


bool TopDileptonEventSaver::passEMu(){

  //previous emu cutflow (minus jets)
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}

bool TopDileptonEventSaver::passEE(){

  //previous ee cutflow (minus jets)
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 11 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5 || (abs(m_lep_eta[1]) < 1.52 && abs(m_lep_eta[1]) > 1.37))
    return false;

  return true;
}

bool TopDileptonEventSaver::passMuMu(){

  //previous mumu cutflow (minus jets)
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 13 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5)
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}


bool TopDileptonEventSaver::passEMu_pretag(){

  //previous emu pretag cutflow....
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}

bool TopDileptonEventSaver::passEE_pretag(){

  //previous ee pretag cutflow...
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 11 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5 || (abs(m_lep_eta[1]) < 1.52 && abs(m_lep_eta[1]) > 1.37))
    return false;

  return true;
}

bool TopDileptonEventSaver::passMuMu_pretag(){

  //previous mumu pretag cutflow...
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 13 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5)
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}


bool TopDileptonEventSaver::passEMu_control(){

  //previous emu control region...
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] < 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}

bool TopDileptonEventSaver::passEE_control(){

  //previous ee control region...
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 11 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] < 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5 || (abs(m_lep_eta[1]) < 1.52 && abs(m_lep_eta[1]) > 1.37))
    return false;

  return true;
}

bool TopDileptonEventSaver::passMuMu_control(){

  //previous mumu control region...
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 13 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] < 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5)
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}



void TopDileptonEventSaver::setupPartonLevelEvent(){

  //setting up parton level branches...
  m_truthTreeManager->makeOutputVariable(m_parton_l_pt,            "d_l_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_l_eta,           "d_l_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_l_phi,           "d_l_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_l_e,             "d_l_e");
  m_truthTreeManager->makeOutputVariable(m_parton_l_pdgId,         "d_l_pdgId");
    
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_pt,         "d_lbar_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_eta,        "d_lbar_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_phi,        "d_lbar_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_e,          "d_lbar_e");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_pdgId,      "d_lbar_pdgId");

}

void TopDileptonEventSaver::initializePartonLevelEvent(){

  //initialising parton level branches...
  
  m_parton_l_pt            = -99.;
  m_parton_l_eta           = -99.;
  m_parton_l_phi           = -99.;
  m_parton_l_e             = -99.;
  m_parton_l_pdgId         = 0;
    
  m_parton_lbar_pt         = -99.;
  m_parton_lbar_eta        = -99.;
  m_parton_lbar_phi        = -99.;
  m_parton_lbar_e          = -99.;
  m_parton_lbar_pdgId      = 0;

}

void TopDileptonEventSaver::saveTruthEvent(){

  //saving parton level stuff...

  ///-- Reset everything --///
  initializePartonLevelEvent();

  const xAOD::EventInfo* eventInfo(nullptr);
  top::check( evtStore()->retrieve(eventInfo, m_config->sgKeyEventInfo()) , "Failed to retrieve EventInfo" );
  
  const xAOD::TruthEventContainer * truthEvent(nullptr);
  top::check( evtStore()->retrieve(truthEvent, m_config->sgKeyTruthEvent()) , "Failed to retrieve truth event container" );
  unsigned int truthEventSize = truthEvent->size();
  top::check( truthEventSize==1 , "Failed to retrieve truth PDF info - truth event container size is different from 1 (strange)" );
  
  ///-- Try to get the tops after FSR --///
  const xAOD::PartonHistoryContainer* topPartonCont = nullptr;
  top::check(evtStore()->event()->retrieve(topPartonCont, "TopPartonHistory"), "FAILURE");
  const xAOD::PartonHistory *topParton = topPartonCont->at(0);
 
  bool l_set    = false;
  bool lbar_set = false;

  if (topParton->auxdata<int>("MC_Wdecay1_from_t_pdgId") == -11 || topParton->auxdata<int>("MC_Wdecay1_from_t_pdgId") == -13 || topParton->auxdata<int>("MC_Wdecay1_from_t_pdgId") == -15){
    m_parton_lbar_pdgId = topParton->auxdata<int>("MC_Wdecay1_from_t_pdgId");
    m_parton_lbar.SetPtEtaPhiM(topParton->auxdata<float>("MC_Wdecay1_from_t_pt"),
			       topParton->auxdata<float>("MC_Wdecay1_from_t_eta"),
			       topParton->auxdata<float>("MC_Wdecay1_from_t_phi"),
			       topParton->auxdata<float>("MC_Wdecay1_from_t_m"));
    lbar_set = true;
  }
  if (topParton->auxdata<int>("MC_Wdecay2_from_t_pdgId") == -11 || topParton->auxdata<int>("MC_Wdecay2_from_t_pdgId") == -13 || topParton->auxdata<int>("MC_Wdecay2_from_t_pdgId") == -15){
    m_parton_lbar_pdgId = topParton->auxdata<int>("MC_Wdecay2_from_t_pdgId");
    m_parton_lbar.SetPtEtaPhiM(topParton->auxdata<float>("MC_Wdecay2_from_t_pt"),
			       topParton->auxdata<float>("MC_Wdecay2_from_t_eta"),
			       topParton->auxdata<float>("MC_Wdecay2_from_t_phi"),
			       topParton->auxdata<float>("MC_Wdecay2_from_t_m"));
    lbar_set = true;
  }
  if (topParton->auxdata<int>("MC_Wdecay1_from_tbar_pdgId") == 11 || topParton->auxdata<int>("MC_Wdecay1_from_tbar_pdgId") == 13 || topParton->auxdata<int>("MC_Wdecay1_from_tbar_pdgId") == 15){
    m_parton_l_pdgId = topParton->auxdata<int>("MC_Wdecay1_from_tbar_pdgId");
    m_parton_l.SetPtEtaPhiM(topParton->auxdata<float>("MC_Wdecay1_from_tbar_pt"),
                            topParton->auxdata<float>("MC_Wdecay1_from_tbar_eta"),
                            topParton->auxdata<float>("MC_Wdecay1_from_tbar_phi"),
                            topParton->auxdata<float>("MC_Wdecay1_from_tbar_m"));
    l_set = true;
  }
  if (topParton->auxdata<int>("MC_Wdecay2_from_tbar_pdgId") == 11 || topParton->auxdata<int>("MC_Wdecay2_from_tbar_pdgId") == 13 || topParton->auxdata<int>("MC_Wdecay2_from_tbar_pdgId") == 15){
    m_parton_l_pdgId = topParton->auxdata<int>("MC_Wdecay2_from_tbar_pdgId");
    m_parton_l.SetPtEtaPhiM(topParton->auxdata<float>("MC_Wdecay2_from_tbar_pt"),
                            topParton->auxdata<float>("MC_Wdecay2_from_tbar_eta"),
                            topParton->auxdata<float>("MC_Wdecay2_from_tbar_phi"),
                            topParton->auxdata<float>("MC_Wdecay2_from_tbar_m"));
    l_set = true;
  }

  if (l_set && lbar_set){
    m_parton_l_pt      = m_parton_l.Pt()/1000.;
    m_parton_l_eta     = m_parton_l.Eta();
    m_parton_l_phi     = m_parton_l.Phi();
    m_parton_l_e       = m_parton_l.E()/1000.;
    
    m_parton_lbar_pt   = m_parton_lbar.Pt()/1000.;
    m_parton_lbar_eta  = m_parton_lbar.Eta();
    m_parton_lbar_phi  = m_parton_lbar.Phi();
    m_parton_lbar_e    = m_parton_lbar.E()/1000.;
  }

    ///-- only write out info if objects pass fiducial requirements --///
  //emu
  if( (abs(m_parton_l_pdgId) == 11 && abs(m_parton_lbar_pdgId) == 13) ||
      (abs(m_parton_l_pdgId) == 13 && abs(m_parton_lbar_pdgId) == 11)){
    EventSaverFlatNtuple::saveTruthEvent();
  }
  //ee
  if(abs(m_parton_l_pdgId) == 11 && abs(m_parton_lbar_pdgId) == 11){
    EventSaverFlatNtuple::saveTruthEvent();
  }
  //mumu
  if(abs(m_parton_l_pdgId) == 13 && abs(m_parton_lbar_pdgId) == 13){
    EventSaverFlatNtuple::saveTruthEvent();
  }
  //taue
  if( (abs(m_parton_l_pdgId) == 11 && abs(m_parton_lbar_pdgId) == 15) ||
      (abs(m_parton_l_pdgId) == 15 && abs(m_parton_lbar_pdgId) == 11)){
    EventSaverFlatNtuple::saveTruthEvent();
  }
  //taumu
  if( (abs(m_parton_l_pdgId) == 15 && abs(m_parton_lbar_pdgId) == 13) ||
      (abs(m_parton_l_pdgId) == 13 && abs(m_parton_lbar_pdgId) == 15)){
    EventSaverFlatNtuple::saveTruthEvent();
  }
  //tautau
  if(abs(m_parton_l_pdgId) == 15 && abs(m_parton_lbar_pdgId) == 15){
    EventSaverFlatNtuple::saveTruthEvent();
  }
  
}

void TopDileptonEventSaver::saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent){
  
  //save particle level stuff

  //initialise particle level stuff and move on...
  initializeParticleLevelBranches();
  
  if (!is_MC )
    return;


  TLorentzVector lep_pos, lep_neg;

  //////////////////////////////////
  ///--    Particle leptons    --///
  //////////////////////////////////

  ///-- electrons --///
  for (const auto & elPtr : * plEvent.m_electrons) {
    m_particle_lep_type.push_back(11);
    m_particle_lep_pt.push_back(elPtr->pt()/1000.);
    m_particle_lep_eta.push_back(elPtr->eta());
    m_particle_lep_phi.push_back(elPtr->phi());
    m_particle_lep_e.push_back(elPtr->e()/1000.);
    m_particle_lep_charge.push_back(elPtr->charge());
    ++m_particle_lep_n;
  }
  
  ///-- muons --///
  for (const auto & muPtr : * plEvent.m_muons) {
    m_particle_lep_type.push_back(13);
    m_particle_lep_pt.push_back(muPtr->pt()/1000.);
    m_particle_lep_eta.push_back(muPtr->eta());
    m_particle_lep_phi.push_back(muPtr->phi());
    m_particle_lep_e.push_back(muPtr->e()/1000.);
    m_particle_lep_charge.push_back(muPtr->charge());
    ++m_particle_lep_n;
  }

  if (m_particle_lep_n == 2){
    if (m_particle_lep_charge[0] > 0 && m_particle_lep_charge[1] < 0){
      lep_pos.SetPtEtaPhiE(m_particle_lep_pt[0], m_particle_lep_eta[0], m_particle_lep_phi[0], m_particle_lep_e[0]);
      lep_neg.SetPtEtaPhiE(m_particle_lep_pt[1], m_particle_lep_eta[1], m_particle_lep_phi[1], m_particle_lep_e[1]);
    } else if (m_particle_lep_charge[0] < 0 && m_particle_lep_charge[1] > 0){
      lep_pos.SetPtEtaPhiE(m_particle_lep_pt[1], m_particle_lep_eta[1], m_particle_lep_phi[1], m_particle_lep_e[1]);
      lep_neg.SetPtEtaPhiE(m_particle_lep_pt[0], m_particle_lep_eta[0], m_particle_lep_phi[0], m_particle_lep_e[0]);
    } else {
      std::cout << "WARNING: Particle same-sign lepton event!" << std::endl;
    }
   
  }
  
  //particle level cutflows? Weird place for them

  if (m_particle_lep_n == 2 && 
      ((abs(m_particle_lep_type[0]) == 11 && abs(m_particle_lep_type[1]) == 13) ||
       (abs(m_particle_lep_type[1]) == 11 && abs(m_particle_lep_type[0]) == 13)) && 
      ((m_particle_lep_pt[0] >= 27. && m_particle_lep_pt[1] >= 25.) ||
       (m_particle_lep_pt[0] >= 25. && m_particle_lep_pt[0] >= 27.)) &&
      abs(m_particle_lep_eta[0]) <= 2.5 && abs(m_particle_lep_eta[1]) <= 2.5 && 
      (m_particle_lep_charge[0] * m_particle_lep_charge[1] < 0)){
    //m_particle_pass_emu = true;
  }
  
  if (m_particle_lep_n == 2 &&
      ((abs(m_particle_lep_type[0]) == 11 && abs(m_particle_lep_type[1]) == 11)) &&
       ((m_particle_lep_pt[0] >= 27. && m_particle_lep_pt[1] >= 25.) ||
	(m_particle_lep_pt[0] >= 25. && m_particle_lep_pt[0] >= 27.)) &&
       abs(m_particle_lep_eta[0]) <= 2.5 && abs(m_particle_lep_eta[1]) <= 2.5 &&
      (m_particle_lep_charge[0] * m_particle_lep_charge[1] < 0)){
    //m_particle_pass_ee = true;
  }

  if (m_particle_lep_n == 2 &&
      ((abs(m_particle_lep_type[0]) == 13 && abs(m_particle_lep_type[1]) == 13)) &&
      ((m_particle_lep_pt[0] >= 27. && m_particle_lep_pt[1] >= 25.) ||
       (m_particle_lep_pt[0] >= 25. && m_particle_lep_pt[0] >= 27.)) &&
      abs(m_particle_lep_eta[0]) <= 2.5 && abs(m_particle_lep_eta[1]) <= 2.5 &&
      (m_particle_lep_charge[0] * m_particle_lep_charge[1] < 0)){
    //m_particle_pass_mumu = true;
  }
  
  //if(m_particle_pass_emu || m_particle_pass_ee || m_particle_pass_mumu){
    
    //this was a pseudo top reconstruction, but I removed it
  
  //}
  top::EventSaverFlatNtuple::saveParticleLevelEvent(plEvent);
  
}


void TopDileptonEventSaver::initializeRecoLevelBranches(){

  //initialise reco level stuff
 
  ///-- Lepton Parameters --///
  m_lep_n             = 0;
  
  m_lep_pt.clear();
  m_lep_eta.clear();
  m_lep_phi.clear();
  m_lep_e.clear();
  m_lep_type.clear();
  m_lep_charge.clear();
  m_lep_iso_gradient.clear();
  m_lep_iso_gradient_loose.clear();
  m_lep_id_medium.clear();
  m_lep_id_tight.clear(); 
 
  m_lep_truth_type.clear();
}

void TopDileptonEventSaver::initializeParticleLevelBranches(){

  //initalise particle level stuff

  m_particle_lep_n           = 0;
  m_particle_lep_pt.clear();
  m_particle_lep_eta.clear();
  m_particle_lep_phi.clear();
  m_particle_lep_e.clear();
  m_particle_lep_charge.clear();
  m_particle_lep_type.clear();

} 


int TopDileptonEventSaver::isBranchStored(top::TreeManager const *treeManager, const std::string &variableName)
{
  /* Remove branches from output n-tuple using pattern matching */

  // individual lepton (debugging) SFs
  if (variableName.find("weight_indiv_SF") != std::string::npos)
    return 0;

  // jet b-tagging variables (no longer needed)
  if (variableName == "jet_ip3dsv1" || variableName == "jet_mv2c00" || variableName == "jet_mv2c20")
    return 0;

  // jet DL1 b-tagging variables and special MV2 which we don't use -> this was in previously because DL1 was broken as hell
  if (variableName.find("jet_DL1") != std::string::npos
      || variableName.find("jet_MV2c10mu") != std::string::npos
      || variableName.find("jet_MV2c100") != std::string::npos
      || variableName.find("jet_MV2cl100") != std::string::npos
      || variableName.find("jet_MV2c10rnn") != std::string::npos)
    return 0;

  // unused top-tagging variables
  if (variableName.find("ljet_isTopTagged") != std::string::npos
      || variableName.find("ljet_isWTagged") != std::string::npos
      || variableName.find("ljet_isZTagged") != std::string::npos
      || variableName == "ljet_sd12")
    return 0;

  // unused KLFitter variables
  if (variableName.find("klfitter") != std::string::npos)
    return 0;

  if(m_skipNominal && treeManager->name() == "nominal")
    return 0;

  return -1;
}


//}
